typedef int32_t l;
typedef uint32_t ul;

l inline lsb(const l n) {
  return ((n) & -(n));
}

ul bitreverse(ul x) {
  x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
	x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
	x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
	x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
	return((x >> 16) | (x << 16));
}

ul bitreverse(ul in) {
  ul out { in };
  for (ul i { 0 }; i < sizeof(ul) * 8; ++i) {
    out <<= 1;
    out |= in & 1;
    in >>= 1;
  }
  return out;
}

// binary logarithm
ul inline lb(l x) {
  ul res { 0 };
  while(x) {
    x >>= 1;
    res++;
  }
  return res;
}

bool inline ispoweroftwo(size_t n) {
  return ((n != 0) && !(n & (n - 1)));
}

// Graycode for 32 bit integers
uint32_t g2b(uint32_t gray)
{
        gray ^= (gray >> 16);
        gray ^= (gray >> 8);
        gray ^= (gray >> 4);
        gray ^= (gray >> 2);
        gray ^= (gray >> 1);
        return(gray);
}

// number of leading zeroes
uint32_t lzc(register uint32_t x)
{
        x |= (x >> 1);
        x |= (x >> 2);
        x |= (x >> 4);
        x |= (x >> 8);
        x |= (x >> 16);
        return(WORDBITS - ones(x));
}

// Next larger power of two
uint32_t nlpo2(register uint32_t x)
{
        x |= (x >> 1);
        x |= (x >> 2);
        x |= (x >> 4);
        x |= (x >> 8);
        x |= (x >> 16);
        return(x+1);
}

// Or just use std::bitset...
uint32_t popcount(register uint32_t x)
{
        x -= ((x >> 1) & 0x55555555);
        x = (((x >> 2) & 0x33333333) + (x & 0x33333333));
        x = (((x >> 4) + x) & 0x0f0f0f0f);
        x += (x >> 8);
        x += (x >> 16);
        return(x & 0x0000003f);
}

// Trailing zeroes
uint32_t tzc(register uint32_t x)
{
        return(popcount((x & -x) - 1));
}
